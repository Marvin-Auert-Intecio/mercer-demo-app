package com.intecio.timewindow;

import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;

public class WebSocketPlanShipment extends WebSocketClient {

    private JSONObject messageObject;
    private String toastMessage = "";
    private String responseType = "";

    public WebSocketPlanShipment(URI serverUri) {
        super(serverUri);
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        Log.e("WEBSOCKET", "PlanShipment onOpen");
    }

    @Override
    public void onMessage(String message) {
        Log.i("WEBSOCKET PlanShipment", message);
        try {
            messageObject = new JSONObject(message);
            responseType = messageObject.getString("type");
            toastMessage = messageObject.getString("message");
        } catch (JSONException je) {

        }
    }

    @Override
    public void onClose(int i, String s, boolean b) {
        Log.i("-", s);
    }

    @Override
    public void onError(Exception e) {
        Log.e("WEBSOCKET", "PlanShipment onError:" + e.toString());
    }

    public String getResponseType() {
        return responseType;
    }

    public String getToastMessage() {
        return toastMessage;
    }
}
