package com.intecio.timewindow;

import android.os.Handler;

import android.util.Log;

public class RunnableUpdateStatus implements Runnable {

    private boolean update = true;
    private Handler handler;
    private InterfacesCallbacks.GetCentralData getCentralData;
    private InterfacesCallbacks.GetWebSocket getWebSocket;
    private InterfacesCallbacks.SetCentralData setCentralData;
    private ObjectDriverData objectDriverData;
    private String shipment;
    private WebSocketGetDriverData webSocketGetDriverData;

    /*
    private String hash;
    private String date;
    private String shipmentIncomplete;
    private String shipmentComplete;
    private int startHour;
    private int endHour;
    private int startMinute;
    private int endMinute;
    private String shipmentDate;
    private String timeWindow;
    private int timeSlotSize;

    private String firstName;
    private String lastName;
    private String licensePlate;
    private String phoneNumber;
    private String trailerLicensePlate;
    private String dplbg = "";
    private String uplbg = "";
    private String dplend= "";
    private String uplend = "";
    private String spediteur = "";
    private String locale = "DE";
    */

    RunnableUpdateStatus(Handler handler,
                         InterfacesCallbacks.GetWebSocket getWebSocket,
                         InterfacesCallbacks.GetCentralData getCentralData,
                         ObjectDriverData objectDriverData) {
        this.handler = handler;
        this.getCentralData = getCentralData;
        this.getWebSocket = getWebSocket;
        this.webSocketGetDriverData = getWebSocket.getWebSocketGetDriverData();
        this.objectDriverData = objectDriverData;
    }

    @Override
    public void run() {
        while (update) {
            shipment = objectDriverData.getShipment();
            if (webSocketGetDriverData.isOpen()) {
                webSocketGetDriverData.send(shipment);
                handler.sendEmptyMessage(1);
            } else {
                handler.sendEmptyMessage(0);
            }

            //Log.e("THREAD", "Update status running");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /*
        while (update) {
            hash = getCentralData.getHash();
            date = getCentralData.getDate();
            shipmentIncomplete = getCentralData.getShipmentIncomplete();
            //Log.i("---", hash + "--" + day + "--" + transport);
            webSocketLoadShipment.send(hash + ";" + date + ";" + shipmentIncomplete + ";" + "DE");
            shipmentComplete = webSocketLoadShipment.getTdlnr();
            startHour = webSocketLoadShipment.getStartHour();
            endHour = webSocketLoadShipment.getEndHour();
            startMinute = webSocketLoadShipment.getStartMinute();
            endMinute = webSocketLoadShipment.getEndMinute();
            shipmentDate = webSocketLoadShipment.getShipmentDate();
            timeWindow = webSocketLoadShipment.getStartTime() + "-"
                    + webSocketLoadShipment.getEndTime() + " - "
                    + shipmentDate;
            timeSlotSize = webSocketLoadShipment.getTimeSlotSize();

            setCentralData.setTimeWindow(startHour, startMinute, endHour, endMinute, timeSlotSize);
            setCentralData.setShipmentDate(shipmentDate);
            /*setCentralData.setObjectStatus("Max Mustermann", "INTECIO99", "INTECIO22",
                    shipmentComplete, shipmentIncomplete, timeWindow,
                    "Musterstadt, Musterstraße 10", "10 t");

            objectStatus.setShipmentComplete(shipmentComplete);
            objectStatus.setTimeWindow(timeWindow);
            if (objectStatus.getStatus().equals("")) {
                objectStatus.setStatus("Freie Fahrt");
            }

            handler.sendEmptyMessage(0);
            Log.e("THREAD", "Update status running");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        */
    }
}
