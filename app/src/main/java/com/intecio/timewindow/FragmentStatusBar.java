package com.intecio.timewindow;


import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class FragmentStatusBar extends Fragment {

    private boolean threadRunning = false;
    private Handler handler;
    private InterfacesCallbacks.GetCentralData getCentralData;
    private ObjectDriverData objectDriverData;
    private RunnableUpdateStatusBar runnableUpdateStatusBar;
    private TextView textViewShipment;
    private Thread threadUpdateStatus;
    private View view;

    public FragmentStatusBar() {
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            getCentralData = (InterfacesCallbacks.GetCentralData) context;
        } catch (ClassCastException cce) {
            throw new ClassCastException((context.toString() + "must implement onEventLogin"));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handler = createHandler();
        objectDriverData = getCentralData.getObjectDriverData();
        runnableUpdateStatusBar = new RunnableUpdateStatusBar(handler, getCentralData, objectDriverData);
        threadUpdateStatus = new Thread(runnableUpdateStatusBar);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_status_bar, container, false);
        textViewShipment = view.findViewById(R.id.textViewTransportValue);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!threadRunning) {
            threadUpdateStatus.start();
            threadRunning = true;
        }
    }

    private Handler createHandler() {
        Handler handler = new Handler(Looper.myLooper()) {
            @Override
            public void handleMessage(Message message) {
                updateUI(message.what);
            }
        };
        return handler;
    }

    private void updateUI(int what) {
        if (what == 0) {
            textViewShipment.setText(objectDriverData.getShipment());
        }
    }
}

