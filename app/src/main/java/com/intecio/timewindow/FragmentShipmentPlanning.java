package com.intecio.timewindow;

import android.app.DatePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class FragmentShipmentPlanning extends Fragment {

    private BottomNavigationView bottomNavigationView;
    private Button buttonDate;
    private Button buttonEarlier;
    private Button buttonLater;
    private Button buttonConfirm;
    private Calendar calendar;
    private Calendar pickedCalender;
    private Context context;
    private int index = 12;
    private int timeSlots = 24;
    private InterfacesCallbacks.GetWebSocket getWebSocket;
    private InterfacesCallbacks.GetCentralData getCentralData;
    private InterfacesCallbacks.SetCentralData setCentralData;
    private InterfacesCallbacks.FragmentInteraction fragmentInteraction;
    private List<String> intervalList;
    private ObjectDriverData objectDriverData;
    private RadioButton radioButtonInterval01;
    private RadioButton radioButtonInterval02;
    private RadioButton radioButtonInterval03;
    private RadioButton radioButtonInterval04;
    private RadioButton radioButtonInterval05;
    private RadioGroup radioGroupInterval;
    private SimpleDateFormat simpleDateFormat;
    private String interval01;
    private String interval02;
    private String interval03;
    private String interval04;
    private String interval05;
    private String shipmentDate;
    private String dateString;
    private String interval;
    private String shipment;
    private String responseType = "";
    private String toastMessage = "";
    private String[] dateSplit;
    private String[] intervalSplit;
    private WebSocketPlanShipment webSocketPlanShipment;
    private View view;

    /*
    private int day, month, year;
    private int[] timeWindows;
    private String[] split;
    private String demoIntervalls = "1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1;1";
    private int intervalIndex, listSize;
    private int startHour, startMinute, endHour, endMinute, timeSlots;
    private WebSocketLoadShipment webSocketLoadShipment;
    */

    public FragmentShipmentPlanning() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        getWebSocket = (InterfacesCallbacks.GetWebSocket) context;
        getCentralData = (InterfacesCallbacks.GetCentralData) context;
        setCentralData = (InterfacesCallbacks.SetCentralData) context;
        fragmentInteraction = (InterfacesCallbacks.FragmentInteraction) context;
        this.context = context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        calendar = Calendar.getInstance();
        simpleDateFormat = new SimpleDateFormat("dd.MM.yyyy");
        intervalList = new ArrayList<>();
        webSocketPlanShipment = getWebSocket.getWebSocketPlanShipment();
        objectDriverData = getCentralData.getObjectDriverData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_shipment_planning, container, false);
        buttonDate              = view.findViewById(R.id.buttonDate);
        buttonEarlier           = view.findViewById(R.id.buttonEarlier);
        buttonLater             = view.findViewById(R.id.buttonLater);
        buttonConfirm           = view.findViewById(R.id.buttonConfirmPlanning);
        radioGroupInterval      = view.findViewById(R.id.radioGroupTimeIntervals);
        radioButtonInterval01   = view.findViewById(R.id.radioButtonInterval01);
        radioButtonInterval02   = view.findViewById(R.id.radioButtonInterval02);
        radioButtonInterval03   = view.findViewById(R.id.radioButtonInterval03);
        radioButtonInterval04   = view.findViewById(R.id.radioButtonInterval04);
        radioButtonInterval05   = view.findViewById(R.id.radioButtonInterval05);
        bottomNavigationView    = view.findViewById(R.id.navigationMain);

        radioButtonInterval03.setChecked(true);

        buttonDate.setOnClickListener(OnClickListenerDate);
        buttonEarlier.setOnClickListener(onClickListenerEarlier);
        buttonLater.setOnClickListener(onClickListenerLater);
        buttonConfirm.setOnClickListener(onClickListenerConfirm);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        shipmentDate = objectDriverData.getDplbg();
        buttonDate.setText(shipmentDate);
        setIntervals();
        if (!webSocketPlanShipment.isOpen()) {
            buttonDate.setEnabled(false);
            fragmentInteraction.showToast("Keine Verbindung zum Server, buchen nicht möglich!");
        } else {
            buttonDate.setEnabled(true);
        }
    }

    private void setIntervals() {
        String time;
        for (int i = 0; i < 24; i++) {
            time = String.format("%02d", i) + ":00 - " + String.format("%02d", i+1) + ":00";
            intervalList.add(time);
        }
        setIntervalTexts(timeSlots, index);
    }
/*
    private void setIntervals() {
        String time;
        switch (timeSlots) {
            case 24:
                for (int i = 0; i < 24; i++) {
                    if (i == startHour - 1) {
                        time = i + ":00 - " + "00:00";
                        intervalList.add(time);
                    } else {
                        time = String.format("%02d", i) + ":00 - " + String.format("%02d", i+1) + ":00";
                        intervalList.add(time);
                    }
                }
                index = 11;
                setIntervalTexts(timeSlots, index);
                break;
        }
    }
    */


    private void setIntervalTexts(int size, int index) {
        if (index < 5) {
            interval01 = intervalList.get(index);
            interval02 = intervalList.get(index + 1);
            interval03 = intervalList.get(index + 2);
            interval04 = intervalList.get(index + 3);
            interval05 = intervalList.get(index + 4);
        } else if (index > size - 5) {
            interval01 = intervalList.get(index - 4);
            interval02 = intervalList.get(index - 3);
            interval03 = intervalList.get(index - 2);
            interval04 = intervalList.get(index - 1);
            interval05 = intervalList.get(index);
        } else {
            interval01 = intervalList.get(index - 2);
            interval02 = intervalList.get(index - 1);
            interval03 = intervalList.get(index);
            interval04 = intervalList.get(index + 1);
            interval05 = intervalList.get(index + 2);
        }
        radioButtonInterval01.setText(interval01);
        radioButtonInterval02.setText(interval02);
        radioButtonInterval03.setText(interval03);
        radioButtonInterval04.setText(interval04);
        radioButtonInterval05.setText(interval05);
    }

    private View.OnClickListener OnClickListenerDate = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            pickedCalender = Calendar.getInstance();
                            pickedCalender.set(year, month, dayOfMonth);
                            buttonDate.setText(simpleDateFormat.format(pickedCalender.getTime()));
                        }
                    }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }
    };

    private View.OnClickListener onClickListenerEarlier = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (index > 0) {
                index--;
            }
            Log.i("---", Integer.toString(index));
            setIntervalTexts(timeSlots, index);
        }
    };

    private View.OnClickListener onClickListenerLater = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (index < timeSlots - 1) {
                index++;
            }
            Log.i("---", Integer.toString(index));
            setIntervalTexts(timeSlots, index);
        }
    };



    private View.OnClickListener onClickListenerConfirm = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            //webSocketPlanShipment.send("2020-05-20T16:00:00.000Z;1245;;;;DE");
            shipment = objectDriverData.getShipment();
            dateString = buttonDate.getText().toString();
            dateSplit = dateString.split("\\.");
            String temp =  dateSplit[0];
            dateSplit[0] = dateSplit[2];
            dateSplit[2] = temp;
            dateString = dateSplit[0] + "-" + dateSplit[1] + "-" + dateSplit[2];

            int id = radioGroupInterval.getCheckedRadioButtonId();
            int time = 0;
            String tempSplit[];

            interval = ((RadioButton) view.findViewById(id)).getText().toString();
            intervalSplit = interval.split("\\-");
            tempSplit = intervalSplit[0].trim().split(":");
            if (!tempSplit[0].contentEquals("00")) {
                try {
                    time = Integer.parseInt(tempSplit[0]);
                    time--;
                    intervalSplit[0] = time + ":00";
                } catch (Exception e) {
                    Log.e("-", e.toString());
                }
            }
            interval = intervalSplit[0].trim() + ":00";

            //datePIN = dateString.replace("-", "");
            //setCentralData.setDate(datePIN);
            //webSocketPlanShipment.send(dateString + "T" + interval + ".000Z;" + shipment + ";;;;DE");
            //Log.i("PLANNING", dateString + "T" + interval + ".000Z;1245;;;;DE");
            if (webSocketPlanShipment.isOpen()) {
                webSocketPlanShipment.send(dateString + "T" + interval + ".000Z;" + shipment + ";;;;DE");
                while (responseType.equals("")) {
                    responseType = webSocketPlanShipment.getResponseType();
                    //toastMessage = webSocketPlanShipment.getToastMessage();
                }
                //toast = Toast.makeText(context, toastMessage, Toast.LENGTH_LONG);
                if (responseType.equals("S")) {
                    toastMessage = "Transport " + shipment + " erfolgreich geplant";
                    fragmentInteraction.showToast(toastMessage);
                    fragmentInteraction.setBottomMenuItem(R.id.navigationOrder);
                    fragmentInteraction.changeMainFragment("fragmentStatus");
                }
                if (responseType.equals("E")) {
                    toastMessage = "Fehler beim Planen von " + shipment;
                    fragmentInteraction.showToast(toastMessage);
                }
            } else {
                fragmentInteraction.showToast("Fehler: keine Serververbindung, Zeit buchen nicht möglich");
            }
        }
    };
}
