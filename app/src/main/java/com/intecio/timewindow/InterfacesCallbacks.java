package com.intecio.timewindow;

public class InterfacesCallbacks {

    interface GetWebSocket{
        WebSocketPlanShipment getWebSocketPlanShipment();
        WebSocketSetDriverData getWebSocketSetDriverData();
        WebSocketGetDriverData getWebSocketGetDriverData();
        WebSocketLoadChatMessages getWebSocketLoadChatMessages();
        WebSocketWriteChatMessage getWebSocketWriteChatMessages();
    }

    interface FragmentInteraction {
        void showStatusBar();
        void hideStatusBar();
        void showNavigation();
        void hideNavigation();
        void setNavigationItem(int id);
        void changeMainFragment(String fragmentName);
        void setBottomMenuItem(int id);
        void showToast(String toastMessage);
    }

    interface GetCentralData {
        int getDisplayWidth();
        int getDisplayHeight();
        ObjectDriverData getObjectDriverData();
        ObjectChatMessage getObjectChatMessage();
    }

    interface SetCentralData {}
}


