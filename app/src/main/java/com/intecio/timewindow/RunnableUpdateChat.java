package com.intecio.timewindow;

import android.os.Handler;

import java.util.List;

public class RunnableUpdateChat implements Runnable {

    private boolean update = true;
    private Handler handler;
    private InterfacesCallbacks.GetCentralData getCentralData;
    private InterfacesCallbacks.GetWebSocket getWebSocket;
    private List<ObjectChatMessage> listObjectMessages;
    private ObjectDriverData objectDriverData;
    private String shipment;
    private WebSocketGetDriverData webSocketGetDriverData;
    private WebSocketLoadChatMessages webSocketLoadChatMessages;

    RunnableUpdateChat(Handler handler,
                       //List<ObjectChatMessage> listObjectMessages,
                       InterfacesCallbacks.GetWebSocket getWebSocket,
                       InterfacesCallbacks.GetCentralData getCentralData) {
        this.handler                    = handler;
        this.getCentralData             = getCentralData;
        this.getWebSocket               = getWebSocket;
        this.webSocketLoadChatMessages  = getWebSocket.getWebSocketLoadChatMessages();
        this.objectDriverData           = getCentralData.getObjectDriverData();
    }

    @Override
    public void run() {
        while (update) {
            shipment = objectDriverData.getShipment();
            if (webSocketLoadChatMessages.isOpen()) {
                webSocketLoadChatMessages.send(shipment);
                //while (!webSocketLoadChatMessages.isListReady()) {}
                handler.sendEmptyMessage(1);
                if (webSocketLoadChatMessages.getListReady()) {
                    handler.sendEmptyMessage(2);
                }
            } else {
                handler.sendEmptyMessage(0);
            }

            //Log.e("THREAD", "Update status running");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        /*
        while (update) {
            hash = getCentralData.getHash();
            date = getCentralData.getDate();
            shipmentIncomplete = getCentralData.getShipmentIncomplete();
            //Log.i("---", hash + "--" + day + "--" + transport);
            webSocketLoadShipment.send(hash + ";" + date + ";" + shipmentIncomplete + ";" + "DE");
            shipmentComplete = webSocketLoadShipment.getTdlnr();
            startHour = webSocketLoadShipment.getStartHour();
            endHour = webSocketLoadShipment.getEndHour();
            startMinute = webSocketLoadShipment.getStartMinute();
            endMinute = webSocketLoadShipment.getEndMinute();
            shipmentDate = webSocketLoadShipment.getShipmentDate();
            timeWindow = webSocketLoadShipment.getStartTime() + "-"
                    + webSocketLoadShipment.getEndTime() + " - "
                    + shipmentDate;
            timeSlotSize = webSocketLoadShipment.getTimeSlotSize();

            setCentralData.setTimeWindow(startHour, startMinute, endHour, endMinute, timeSlotSize);
            setCentralData.setShipmentDate(shipmentDate);
            /*setCentralData.setObjectStatus("Max Mustermann", "INTECIO99", "INTECIO22",
                    shipmentComplete, shipmentIncomplete, timeWindow,
                    "Musterstadt, Musterstraße 10", "10 t");

            objectStatus.setShipmentComplete(shipmentComplete);
            objectStatus.setTimeWindow(timeWindow);
            if (objectStatus.getStatus().equals("")) {
                objectStatus.setStatus("Freie Fahrt");
            }

            handler.sendEmptyMessage(0);
            Log.e("THREAD", "Update status running");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        */
    }
}
