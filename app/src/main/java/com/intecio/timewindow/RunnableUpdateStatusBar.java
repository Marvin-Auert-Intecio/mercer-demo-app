package com.intecio.timewindow;

import android.os.Handler;
import android.util.Log;

public class RunnableUpdateStatusBar implements Runnable {

    private boolean update = true;
    private Handler handler;
    private InterfacesCallbacks.GetCentralData getCentralData;
    private ObjectDriverData objectDriverData;
    private String status;

    RunnableUpdateStatusBar(Handler handler,
                            InterfacesCallbacks.GetCentralData getCentralData,
                            ObjectDriverData objectDriverData) {
        this.handler = handler;
        this.getCentralData = getCentralData;
        this.objectDriverData = objectDriverData;
    }

    @Override
    public void run() {
        while (update) {
            status = objectDriverData.getStatus();
            handler.sendEmptyMessage(0);
            //Log.e("THREAD", "Update status bar running");
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
