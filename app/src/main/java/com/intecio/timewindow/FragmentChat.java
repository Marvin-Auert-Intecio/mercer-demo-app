package com.intecio.timewindow;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FragmentChat extends Fragment {

    private boolean threadRunning = false;
    private Button buttonSendMessage;
    private Context context;
    private EditText editTextMessage;
    private Handler handler;
    private InterfacesCallbacks.GetWebSocket getWebSocket;
    private InterfacesCallbacks.GetCentralData getCentralData;
    private InterfacesCallbacks.FragmentInteraction fragmentInteraction;
    private List<ObjectChatMessage> listObjectMessages;
    private NestedScrollView nestedScrollView;
    private ObjectDriverData objectDriverData;
    private ObjectChatMessage objectChatMessage;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RunnableUpdateChat runnableUpdateChat;
    private String shipment = "";
    private String name = "";
    private String chatMessage = "";
    private Thread threadUpdateChat;
    private View view;
    private WebSocketGetDriverData webSocketGetDriverData;
    private WebSocketLoadChatMessages webSocketLoadChatMessages;
    private WebSocketWriteChatMessage webSocketWriteChatMessage;


    public FragmentChat() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
        try {
            getWebSocket = (InterfacesCallbacks.GetWebSocket) context;
            getCentralData = (InterfacesCallbacks.GetCentralData) context;
            fragmentInteraction = (InterfacesCallbacks.FragmentInteraction) context;
        } catch (ClassCastException cce) {
            throw new ClassCastException((context.toString() + "must implement onEventLogin"));
        }
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        webSocketWriteChatMessage = getWebSocket.getWebSocketWriteChatMessages();
        webSocketLoadChatMessages = getWebSocket.getWebSocketLoadChatMessages();
        objectDriverData = getCentralData.getObjectDriverData();
        objectChatMessage = getCentralData.getObjectChatMessage();
        //shipment = objectDriverData.getShipment();
        listObjectMessages = webSocketLoadChatMessages.getListObjectMessage();

        handler = createHandler();
        runnableUpdateChat = new RunnableUpdateChat(handler, getWebSocket, getCentralData);
        threadUpdateChat = new Thread(runnableUpdateChat);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_chat, container, false);
        buttonSendMessage   = view.findViewById(R.id.buttonSendMessage);
        editTextMessage     = view.findViewById(R.id.editTextMessage);
        nestedScrollView    = view.findViewById(R.id.nestedScrollViewChat);
        recyclerView        = view.findViewById(R.id.recyclerViewChat);
        layoutManager       = new LinearLayoutManager(context);
        adapter             = new AdapterChatMessages(context, listObjectMessages);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
        buttonSendMessage.setOnClickListener(onClickWriteMessage);;
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!threadRunning) {
            threadUpdateChat.start();
            threadRunning = true;
        }
    }

    private Handler createHandler() {
        Handler handler = new Handler(Looper.myLooper()) {
            @Override
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                        fragmentInteraction.showToast("Keine Verbindung zum Server, Chat aktualisieren nicht möglich");
                        break;
                    case 1:
                        updateUI();
                        break;
                    case 2:
                        nestedScrollView.post(new Runnable() {
                            public void run() {
                                nestedScrollView.fullScroll(nestedScrollView.FOCUS_DOWN);
                            }
                        });
                }
            }
        };
        return handler;
    }

    private void updateUI() {
        listObjectMessages = webSocketLoadChatMessages.getListObjectMessage();
        if (!listObjectMessages.isEmpty()) {
            adapter.notifyDataSetChanged();
        }
    }

    private View.OnClickListener onClickWriteMessage = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            shipment = objectDriverData.getShipment();
            name = objectDriverData.getFirstName() + " " + objectDriverData.getLastName();
            chatMessage = editTextMessage.getText().toString();
            if (webSocketLoadChatMessages.isOpen() && !chatMessage.isEmpty()) {
                webSocketWriteChatMessage.send(shipment + ";" + name + ";" + chatMessage);
            }
            //webSocketWriteChatMessage.send("1245;Max;Hallo Welt");
        }
    };
}
