package com.intecio.timewindow;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;

import java.net.URI;
import java.net.URISyntaxException;

public class ActivityMain extends AppCompatActivity
    implements
        InterfacesCallbacks.GetCentralData,
        InterfacesCallbacks.SetCentralData,
        InterfacesCallbacks.GetWebSocket,
        InterfacesCallbacks.FragmentInteraction{

    private BottomNavigationView bottomNavigationView;
    private Context context;
    private DisplayMetrics displayMetrics;
    private Fragment fragmentBreakRecording;
    private Fragment fragmentDriverRegistration;
    private Fragment fragmentChat;
    private Fragment fragmentLogin;
    private Fragment fragmentQrCode;
    private Fragment fragmentStatus;
    private Fragment fragmentStatusBar;
    private Fragment fragmentShipmentPlanning;
    private FragmentManager fragmentManager;
    private FragmentManager statusManager;
    private int displayWidth = 0;
    private int displayHeigth = 0;
    private ObjectStatus objectStatus;
    private ObjectChatMessage objectChatMessage;
    private ObjectDriverData objectDriverData;
    private Toast toast;
    private String toastMessage = "";
    private String path = "wss://timewindows0017135848trial.hanatrial.ondemand.com/TimeWindow/";
    //private String path = "ws://192.168.8.143:8080/TimeWindow/";
    //private String path = "ws://192.168.144.120:8080/TimeWindow/";
    private Toolbar mainToolbar;
    private URI uriPlanShipment;
    private URI uriLoadChatMessages;
    private URI uriWriteChatMessages;
    private URI uriSetDriverData;
    private URI uriGetDriverData;
    private WebSocketPlanShipment webSocketPlanShipment;
    private WebSocketLoadChatMessages webSocketLoadChatMessages;
    private WebSocketWriteChatMessage webSocketWriteChatMessage;
    private WebSocketSetDriverData webSocketSetDriverData;
    private WebSocketGetDriverData webSocketGetDriverData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context                 = getApplicationContext();
        displayMetrics          = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        displayWidth            = displayMetrics.widthPixels;
        displayHeigth           = displayMetrics.heightPixels;
        mainToolbar             = findViewById(R.id.toolbarMain);
        bottomNavigationView    = findViewById(R.id.navigationMain);
        bottomNavigationView.setOnNavigationItemSelectedListener(onNavigationListener);
        objectStatus = new ObjectStatus("","","","",
                "","","","","");
        objectChatMessage = new ObjectChatMessage();
        objectDriverData = new ObjectDriverData();
        toast = Toast.makeText(getApplicationContext(), toastMessage, Toast.LENGTH_LONG);

        /*
        try {
            uriLoadShipment = new URI(path + "loadshipments");
            webSocketLoadShipment = new WebSocketLoadShipment(uriLoadShipment);
            webSocketLoadShipment.connect();
        } catch (URISyntaxException e) {
            Log.e("WEBSOCKET", e.toString());
        }
         */
        try {
            uriPlanShipment = new URI(path +"planshipments");
            webSocketPlanShipment = new WebSocketPlanShipment(uriPlanShipment);
            webSocketPlanShipment.connect();
        } catch (URISyntaxException e) {
            Log.e("WEBSOCKET", e.toString());
        }
        try {
            uriLoadChatMessages = new URI(path + "loadchat");
            webSocketLoadChatMessages = new WebSocketLoadChatMessages(uriLoadChatMessages);
            webSocketLoadChatMessages.connect();
        } catch (URISyntaxException e) {
            Log.e("WEBSOCKET", e.toString());
        }
        try {
            uriWriteChatMessages = new URI(path + "writechat");
            webSocketWriteChatMessage = new WebSocketWriteChatMessage(uriWriteChatMessages);
            webSocketWriteChatMessage.connect();
        } catch (URISyntaxException e) {
            Log.e("WEBSOCKET", e.toString());
        }
        try {
            uriSetDriverData = new URI(path + "setdriverdata");
            webSocketSetDriverData = new WebSocketSetDriverData(uriSetDriverData, objectDriverData);
            webSocketSetDriverData.connect();
        } catch (URISyntaxException e) {
            Log.e("WEBSOCKET", e.toString());
        }
        try {
            uriGetDriverData = new URI(path + "getdriverdata");
            webSocketGetDriverData = new WebSocketGetDriverData(uriGetDriverData, objectDriverData);
            webSocketGetDriverData.connect();
        } catch (URISyntaxException e) {
            Log.e("WEBSOCKET", e.toString());
        }

        fragmentBreakRecording = new FragmentBreakRecording();
        fragmentDriverRegistration = new FragmentDriverRegistration();
        fragmentChat = new FragmentChat();
        fragmentLogin = new FragmentLogin();

        fragmentQrCode = new FragmentQrCode();
        fragmentStatus = new FragmentStatus();
        fragmentStatusBar = new FragmentStatusBar();
        fragmentShipmentPlanning = new FragmentShipmentPlanning();

        fragmentManager = getSupportFragmentManager();
        statusManager = getSupportFragmentManager();

        changeFragment("fragmentLogin");
    }

    @Override
    public void onResume() {
        super.onResume();
        mainToolbar.setTitle("Intecio GmbH");
    }

    @Override
    public int getDisplayWidth() {
        return displayWidth;
    }

    @Override
    public int getDisplayHeight() {
        return displayHeigth;
    }

    @Override
    public void showToast(String toastMessage) {
        toast = Toast.makeText(context, toastMessage, Toast.LENGTH_LONG);
        toast.show();
    }

    @Override
    public void setBottomMenuItem(int id) {
        bottomNavigationView.setSelectedItemId(id);
    }

    @Override
    public void changeMainFragment(String fragmentName) {
        changeFragment(fragmentName);
    }

    @Override
    public ObjectDriverData getObjectDriverData() {
        return objectDriverData;
    }

    @Override
    public ObjectChatMessage getObjectChatMessage() {
        return  objectChatMessage;
    }

    @Override
    public void showNavigation() {
        bottomNavigationView.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideNavigation() {
        bottomNavigationView.setVisibility(View.GONE);
    }

    @Override
    public WebSocketPlanShipment getWebSocketPlanShipment() {
        return webSocketPlanShipment;
    }

    @Override
    public WebSocketSetDriverData getWebSocketSetDriverData() {
        return webSocketSetDriverData;
    }

    @Override
    public WebSocketGetDriverData getWebSocketGetDriverData() {
        return webSocketGetDriverData;
    }

    @Override
    public WebSocketLoadChatMessages getWebSocketLoadChatMessages() {
        return webSocketLoadChatMessages;
    }

    @Override
    public WebSocketWriteChatMessage getWebSocketWriteChatMessages() {
        return webSocketWriteChatMessage;
    }

    @Override
    public void showStatusBar() {
        showFragmentStatus();
    }

    @Override
    public void hideStatusBar() {
        hideFragmentStatus();
    }

    @Override
    public void setNavigationItem(int id) {
        bottomNavigationView.setSelectedItemId(id);
    }

    private void changeFragment(String fragmentName) {
        Fragment fragment = null;
        String title = "";
        switch (fragmentName) {
            case "fragmentBreakRecording":
                fragment = fragmentBreakRecording;
                title = "Unterbrechungen";
                break;
            case "fragmentDriverRegistration":
                fragment = fragmentDriverRegistration;
                title = "Daten Registrierung";
                break;
            case "fragmentLogin":
                fragment = fragmentLogin;
                title = "Intecio GmbH - Login";
                break;
            case "fragmentQrCode":
                fragment = fragmentQrCode;
                title = "Check-In";
                break;
            case "fragmentShipmentPlanning":
                fragment = fragmentShipmentPlanning;
                title = "Zeitplanung";
                break;
            case "fragmentStatus":
                fragment = fragmentStatus;
                title = "Auftragsstatus";
                break;
            case "fragmentChat":
                fragment = fragmentChat;
                title = "Nachrichten";
                break;
        }
        if (fragment != null) {
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragmentContainer, fragment);
            //fragmentTransaction.addToBackStack(null);
            fragmentTransaction.commit();
            mainToolbar.setTitle(title);
        }
    }

    private void showFragmentStatus() {
        FragmentTransaction fragmentTransaction = statusManager.beginTransaction();
        fragmentTransaction.replace(R.id.fragmentStatus, fragmentStatusBar);
        fragmentTransaction.show(fragmentStatusBar);
        fragmentTransaction.commit();
    }

    private void hideFragmentStatus() {
        FragmentTransaction fragmentTransaction = statusManager.beginTransaction();
        fragmentTransaction.hide(fragmentStatusBar);
        fragmentTransaction.commit();
    }

    private BottomNavigationView.OnNavigationItemSelectedListener onNavigationListener =
            new BottomNavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
                    switch (menuItem.getItemId()) {
                        case R.id.navigationShipment:
                            changeFragment("fragmentShipmentPlanning");
                            return true;
                        case R.id.navigationBreak:
                            changeFragment("fragmentBreakRecording");
                            return true;
                        case R.id.navigationOrder:
                            changeFragment("fragmentStatus");
                            return true;
                        case R.id.navigationRegistration:
                            changeFragment("fragmentDriverRegistration");
                            return true;
                        case R.id.navigationChat:
                            changeFragment("fragmentChat");
                            return true;
                    }
                    return false;
                }
            };
}
