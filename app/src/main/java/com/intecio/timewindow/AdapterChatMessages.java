package com.intecio.timewindow;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class AdapterChatMessages extends RecyclerView.Adapter<ViewHolderChatMessages> {

    private List<ObjectChatMessage> listMessages;
    private LayoutInflater layoutInflater;

    public AdapterChatMessages(Context context, List<ObjectChatMessage> listMessages) {
        this.layoutInflater = LayoutInflater.from(context);
        this.listMessages = listMessages;
    }

    @Override
    public ViewHolderChatMessages onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.layout_chat_message, parent, false);
        return new ViewHolderChatMessages(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolderChatMessages holder, int position) {
        String chatMessage  = listMessages.get(position).getChatMessage();
        String date         = listMessages.get(position).getDate();
        String time         = listMessages.get(position).getTime();
        String name         = listMessages.get(position).getName();

        holder.chatMessage.setText(chatMessage);
        holder.date.setText(date);
        holder.time.setText(time);
        holder.name.setText(name);
    }

    @Override
    public int getItemCount() {
        return listMessages.size();
    }
}
