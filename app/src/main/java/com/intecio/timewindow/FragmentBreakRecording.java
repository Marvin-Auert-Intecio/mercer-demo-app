package com.intecio.timewindow;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import androidx.fragment.app.Fragment;

public class FragmentBreakRecording extends Fragment {

    private boolean send = false;
    private int hour, minute;
    private Button buttonConfirmBreak;
    private InterfacesCallbacks.SetCentralData setCentralData;
    private InterfacesCallbacks.GetCentralData getCentralData;
    private InterfacesCallbacks.GetWebSocket getWebSocket;
    private InterfacesCallbacks.FragmentInteraction fragmentInteraction;
    private ObjectDriverData objectDriverData;
    private RadioButton radioButtonFreeRide;
    private RadioButton radioButtonBreak;
    private RadioButton radioButtonTrafficJam;
    private RadioButton radioButtonAccident;
    private RadioButton radioButtonPolice;
    private RadioButton radioButtonCustoms;
    private RadioButton radioButtonTemp;
    private RadioGroup radioGroupLeft;
    private RadioGroup radioGroupRight;
    private String chatMessage = "";
    private String status = "";
    private View view;
    private WebSocketWriteChatMessage webSocketWriteChatMessage;

    public FragmentBreakRecording() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            setCentralData = (InterfacesCallbacks.SetCentralData) context;
            getCentralData = (InterfacesCallbacks.GetCentralData) context;
            getWebSocket = (InterfacesCallbacks.GetWebSocket) context;
            fragmentInteraction = (InterfacesCallbacks.FragmentInteraction) context;
        } catch (ClassCastException cce) {
            throw new ClassCastException((context.toString() + "must implement onEventLogin"));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        objectDriverData = getCentralData.getObjectDriverData();
        webSocketWriteChatMessage = getWebSocket.getWebSocketWriteChatMessages();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view =  inflater.inflate(R.layout.fragment_break_recording, container, false);
        radioButtonFreeRide     = view.findViewById(R.id.buttonFreeRide);
        radioButtonBreak        = view.findViewById(R.id.buttonBreak);
        radioButtonTrafficJam   = view.findViewById(R.id.buttonTrafficJam);
        radioButtonAccident     = view.findViewById(R.id.buttonAccident);
        radioButtonPolice       = view.findViewById(R.id.buttonPolice);
        radioButtonCustoms      = view.findViewById(R.id.buttonCustoms);
        radioGroupLeft          = view.findViewById(R.id.radioGroupLeft);
        radioGroupRight         = view.findViewById(R.id.radioGroupRight);
        buttonConfirmBreak      = view.findViewById(R.id.buttonConfirmBreak);

        radioButtonFreeRide.setOnClickListener(onClickListenerEvent);
        radioButtonBreak.setOnClickListener(onClickListenerEvent);
        radioButtonTrafficJam.setOnClickListener(onClickListenerEvent);
        radioButtonAccident.setOnClickListener(onClickListenerEvent);
        radioButtonPolice.setOnClickListener(onClickListenerEvent);
        radioButtonCustoms.setOnClickListener(onClickListenerEvent);
        buttonConfirmBreak.setOnClickListener(onClickListenerConfirm);

        radioButtonFreeRide.setChecked(true);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private View.OnClickListener onClickListenerEvent = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            int id = view.getId();
            radioButtonTemp = (RadioButton) view;
            if (radioButtonTemp.isChecked()) {
                switch (id) {
                    case R.id.buttonFreeRide:
                        status = "Freie Fahrt";
                        chatMessage = "Habe freie Fahrt";
                        radioGroupRight.clearCheck();
                        send = true;
                        break;
                    case R.id.buttonBreak:
                        status = "Pause";
                        chatMessage = "Pause";
                        radioGroupLeft.clearCheck();
                        send = true;
                        break;
                    case R.id.buttonTrafficJam:
                        status = "Stau";
                        chatMessage = "Stehe gerade im Stau";
                        radioGroupRight.clearCheck();
                        send = true;
                        break;
                    case R.id.buttonAccident:
                        status = "Unfall";
                        chatMessage = "Bin in einen Unfall verwickelt";
                        radioGroupLeft.clearCheck();
                        send = true;
                        break;
                    case R.id.buttonPolice:
                        status = "Polizei";
                        chatMessage = "Bin gerade in einer Polizeikontrolle";
                        radioGroupRight.clearCheck();
                        send = true;
                        break;
                    case R.id.buttonCustoms:
                        status = "Zoll";
                        chatMessage = "Bin gerade beim Zoll";
                        radioGroupLeft.clearCheck();
                        send = true;
                        break;
                }
            }
        }
    };

    private View.OnClickListener onClickListenerConfirm = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            objectDriverData.setStatus(status);
            if (webSocketWriteChatMessage.isOpen() && send) {
                webSocketWriteChatMessage.send(objectDriverData.getShipment() + ";" +
                        objectDriverData.getFirstName() + " " + objectDriverData.getLastName() + ";" + chatMessage);
                send = false;
                fragmentInteraction.showToast("Status erfolgreich gemeldet");
            } else if (!send){
                fragmentInteraction.showToast("Bitte erst einen neuen Status auswählen ");
            } else {
                fragmentInteraction.showToast("Fehler: Keine Serververbindung, Meldung nicht mölich");
            }
        }
    };
}
