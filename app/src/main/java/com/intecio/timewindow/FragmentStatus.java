package com.intecio.timewindow;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

public class FragmentStatus extends Fragment {

    private boolean threadRunning = false;
    private Button buttonCheckIn;
    private InterfacesCallbacks.SetCentralData setCentralData;
    private InterfacesCallbacks.GetCentralData getCentralData;
    private InterfacesCallbacks.GetWebSocket getWebSocket;
    private InterfacesCallbacks.FragmentInteraction fragmentInteraction;
    private Handler handler;
    private ObjectDriverData objectDriverData;
    private Runnable runnableUpdateStatus;
    private String startTime = "";
    private String endTime = "";
    private String[] startTimeSplit;
    private String[] endTimeSplit;
    private TextView textViewStatus;
    private TextView textViewDriver;
    private TextView textViewSpediteur;
    private TextView textViewPlate;
    private TextView textViewPlateTrailer;
    private TextView textViewTimeWindow;
    private TextView textViewDestination;
    //private TextView textViewQuantity;
    private Thread threadUpdateStatus;

    private View view;

    public FragmentStatus() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            setCentralData = (InterfacesCallbacks.SetCentralData) context;
            getCentralData = (InterfacesCallbacks.GetCentralData) context;
            getWebSocket = (InterfacesCallbacks.GetWebSocket) context;
            fragmentInteraction = (InterfacesCallbacks.FragmentInteraction) context;
        } catch (ClassCastException cce) {
            throw new ClassCastException((context.toString() + "must implement onEventLogin"));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        objectDriverData = getCentralData.getObjectDriverData();
        handler = createHandler();
        runnableUpdateStatus = new RunnableUpdateStatus(handler, getWebSocket, getCentralData, objectDriverData);
        threadUpdateStatus = new Thread(runnableUpdateStatus);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_status, container, false);
        textViewDriver          = view.findViewById(R.id.textViewStatusDriverShow);
        textViewSpediteur       = view.findViewById(R.id.textViewStatusSpediteurShow);
        textViewPlate           = view.findViewById(R.id.textViewStatusPlateShow);
        textViewPlateTrailer    = view.findViewById(R.id.textViewStatusTrailerShow);
        textViewTimeWindow      = view.findViewById(R.id.textViewStatusTimeWindowShow);
        textViewDestination     = view.findViewById(R.id.textViewStatusDestinationShow);
        //textViewQuantity        = view.findViewById(R.id.textViewStatusQuantityShow);
        textViewStatus          = view.findViewById(R.id.textViewStatusStatusShow);
        buttonCheckIn           = view.findViewById(R.id.buttonCheckIn);
        buttonCheckIn.setOnClickListener(onClickListenerCheckIn);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (!threadRunning) {
            threadUpdateStatus.start();
            threadRunning = true;
        }
    }

    private Handler createHandler() {
        Handler handler = new Handler(Looper.myLooper()) {
            @Override
            public void handleMessage(Message message) {
                switch (message.what) {
                    case 0:
                        fragmentInteraction.showToast("Keine Verbindung zum Server, Statusanzeige nicht möglich");
                        break;
                    case 1:
                        updateUI();
                        break;
                }
                if (message.what == 1) {

                }
            }
        };
        return handler;
    }

    private void updateUI() {
        /*
        if (objectDriverData.getStatus().contentEquals("X")) {
            textViewStatus.setText("Unterbrochen");
        } else {
            textViewStatus.setText("Frei Fahrt");
        }
        */
        textViewStatus.setText(objectDriverData.getStatus());
        textViewDriver.setText(objectDriverData.getFirstName() + " " + objectDriverData.getLastName());
        textViewSpediteur.setText(objectDriverData.getSpediteur());
        textViewPlate.setText(objectDriverData.getLicensePlate());
        textViewPlateTrailer.setText(objectDriverData.getTrailerLicensePlate());

        try {
            startTimeSplit  = objectDriverData.getUplbg().split(":");
            endTimeSplit    = objectDriverData.getUplend().split(":");
            startTime = startTimeSplit[0] + ":" + startTimeSplit[1];
            endTime = endTimeSplit[0] + ":" + endTimeSplit[1];
            textViewTimeWindow.setText(startTime + "-" + endTime + " - " + objectDriverData.getDplbg());
        } catch (Exception e) {
            Log.e("StringSplit", e.toString());
        }


        //textViewDestination.setText(objectStatus.getDestination());
        //textViewQuantity.setText(objectStatus.getQuantity());
    }

    private View.OnClickListener onClickListenerCheckIn = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            fragmentInteraction.changeMainFragment("fragmentQrCode");
        }
    };
}
