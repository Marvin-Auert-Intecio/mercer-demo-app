package com.intecio.timewindow;


import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;

import java.io.File;

public class FragmentQrCode extends Fragment {

    private Bitmap bitmap;
    private BitMatrix bitMatrix;
    private Button buttonQrCodeBack;
    private ImageView imageView;
    private int bitmapWidth = 0;
    private int bitmapHeight = 0;
    private int qrCodeSize = 0;
    private InterfacesCallbacks.GetCentralData getCentralData;
    private InterfacesCallbacks.FragmentInteraction fragmentInteraction;
    private File fileQRCode;
    private ObjectDriverData objectDriverData;
    private String qrCodeText;
    private String filePath = "qrcode.png";
    private String fileType = "png";
    private TextView textView;
    private View view;

    private final int COLORWHITE = 0xFFFFFFFF;
    private final int COLORBLACK = 0xFF000000;

    private DisplayMetrics displayMetrics;
    int displyHeight = 0;
    int displayWidth = 0;

    public FragmentQrCode() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            getCentralData = (InterfacesCallbacks.GetCentralData) context;
            fragmentInteraction = (InterfacesCallbacks.FragmentInteraction) context;
        } catch (ClassCastException cce) {
            throw new ClassCastException((context.toString() + "must implement onEventLogin"));
        }
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
        objectDriverData = getCentralData.getObjectDriverData();
        displayWidth = getCentralData.getDisplayWidth();
        displyHeight = getCentralData.getDisplayHeight();
        qrCodeSize = displayWidth - 200;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_qr_code, container, false);
        imageView = view.findViewById(R.id.imageViewQRCode);
        textView = view.findViewById(R.id.textViewQRCode);
        buttonQrCodeBack = view.findViewById(R.id.buttonQrCodeBack);
        buttonQrCodeBack.setOnClickListener(onClickListenerQrCodeBack);
        /*constraintLayout.post(new Runnable() {
            @Override
            public void run() {
                qrCodeSize = constraintLayout.getMeasuredWidth() - 20;
            }
        });*/
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        qrCodeText = objectDriverData.getYardNumber();
        bitmap = createQRBitmap(qrCodeText);
        textView.setText("Transportnummer: " + qrCodeText + "\nals scanbarer QR-Code");
        imageView.setImageBitmap(bitmap);
    }

    private Bitmap createQRBitmap(String text) {
        try {
            bitMatrix = new MultiFormatWriter().encode(text, BarcodeFormat.QR_CODE, qrCodeSize, qrCodeSize, null);
            bitmapWidth = bitMatrix.getWidth();
            bitmapHeight = bitMatrix.getHeight();
            int[] pixels = new int[bitmapWidth * bitmapHeight];
            for (int h = 0; h < bitmapHeight; h++) {
                int offset = h * bitmapWidth;
                for (int w = 0; w < bitmapWidth; w++) {
                    pixels[offset +w] = bitMatrix.get(w, h) ? COLORBLACK:COLORWHITE;
                }
            }
            bitmap = Bitmap.createBitmap(bitmapWidth, bitmapHeight, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, qrCodeSize, 0,0 ,bitmapWidth, bitmapHeight);
        } catch (Exception e) {
            return null;
        }
        return bitmap;
    }

    private View.OnClickListener onClickListenerQrCodeBack = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            fragmentInteraction.changeMainFragment("fragmentStatus");
        }
    };
}
