package com.intecio.timewindow;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.fragment.app.Fragment;

public class FragmentDriverRegistration extends Fragment {

    private Button buttonConfirmData;
    private EditText editTextRegistrationName;
    private EditText editTextRegistrationFirstName;
    private EditText editTextRegistrationPhone;
    private EditText editTextRegistrationPlate;
    private EditText editTextRegistrationTrailer;
    private InterfacesCallbacks.SetCentralData setCentralData;
    private InterfacesCallbacks.GetCentralData getCentralData;
    private InterfacesCallbacks.GetWebSocket getWebSocket;
    private InterfacesCallbacks.FragmentInteraction fragmentInteraction;
    private ObjectDriverData objectDriverData;
    private String shipment;
    private String firstName;
    private String lastName;
    private String licensePlate;
    private String phone;
    private String trailerLicensePlate;
    private String dplbg = "";
    private String uplbg = "";
    private String dplend= "";
    private String uplend = "";
    private String spediteur = "";
    private String locale = "DE";
    private String responseType = "";
    private String toastMessage;
    private View view;
    private WebSocketSetDriverData webSocketSetDriverData;

    public FragmentDriverRegistration() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            getWebSocket = (InterfacesCallbacks.GetWebSocket) context;
            getCentralData = (InterfacesCallbacks.GetCentralData) context;
            fragmentInteraction = (InterfacesCallbacks.FragmentInteraction) context;
        } catch (ClassCastException cce) {
            throw new ClassCastException((context.toString() + "must implement onEventLogin"));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        objectDriverData = getCentralData.getObjectDriverData();
        webSocketSetDriverData = getWebSocket.getWebSocketSetDriverData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_driver_registration, container, false);
        editTextRegistrationName        = view.findViewById(R.id.editTextRegistrationName);
        editTextRegistrationFirstName   = view.findViewById(R.id.editTextRegistrationFirstName);
        editTextRegistrationPhone       = view.findViewById(R.id.editTextRegistrationPhone);
        editTextRegistrationPlate       = view.findViewById(R.id.editTextRegistrationPlate);
        editTextRegistrationTrailer     = view.findViewById(R.id.editTextRegistrationTrailer);
        buttonConfirmData               = view.findViewById(R.id.buttonConfirmData);
        buttonConfirmData.setOnClickListener(onClickListenerConfirmData);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        editTextRegistrationName.setText(objectDriverData.getLastName());
        editTextRegistrationFirstName.setText(objectDriverData.getFirstName());
        editTextRegistrationPhone.setText(objectDriverData.getPhone());
        editTextRegistrationPlate.setText(objectDriverData.getLicensePlate());
        editTextRegistrationTrailer.setText(objectDriverData.getTrailerLicensePlate());
        shipment = objectDriverData.getShipment();
        if (!webSocketSetDriverData.isOpen()) {
            buttonConfirmData.setEnabled(false);
            fragmentInteraction.showToast("Keine Verbindung zum Server, speichern nicht möglich!");
        } else {
            buttonConfirmData.setEnabled(true);
        }
    }

    private View.OnClickListener onClickListenerConfirmData = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            firstName           = editTextRegistrationFirstName.getText().toString();
            lastName            = editTextRegistrationName.getText().toString();
            licensePlate        = editTextRegistrationPlate.getText().toString();
            phone               = editTextRegistrationPhone.getText().toString();
            trailerLicensePlate = editTextRegistrationTrailer.getText().toString();

            if (webSocketSetDriverData.isOpen()) {
                webSocketSetDriverData.send(shipment + ";" + firstName + ";" + lastName + ";" +
                        licensePlate + ";" + phone + ";" + trailerLicensePlate);
                //webSocketSetDriverData.send("1245;Max;Mustermann;INTECIO99;0123456789;INTECIO66");
                while (responseType.equals("")) {
                    responseType = webSocketSetDriverData.getResponseType();
                }
                if (responseType.equals("S")) {
                    toastMessage = "Daten zum Transport " + shipment + " gespeichert";
                    fragmentInteraction.showToast(toastMessage);
                    fragmentInteraction.setBottomMenuItem(R.id.navigationOrder);
                    fragmentInteraction.changeMainFragment("fragmentStatus");
                }
                if (responseType.equals("E")) {
                    toastMessage = "Fehler beim Speichern der Daten";
                    fragmentInteraction.showToast(toastMessage);
                }
            } else {
                fragmentInteraction.showToast("Fehler: Keine Serververbindung, Daten speichern nicht möglich");
            }
        }
    };
}
