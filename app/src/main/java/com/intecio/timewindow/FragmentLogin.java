package com.intecio.timewindow;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

public class FragmentLogin extends Fragment {

    private Button buttonLogin;
    private EditText editTextShipment;
    private EditText editTextPin;
    private InterfacesCallbacks.GetCentralData getCentralData;
    private InterfacesCallbacks.SetCentralData setCentralData;
    private InterfacesCallbacks.FragmentInteraction fragmentInteraction;
    private InterfacesCallbacks.GetWebSocket getWebSocket;
    private ObjectDriverData objectDriverData;
    private Toast toast;
    private View view;

    public FragmentLogin() {}

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            getWebSocket = (InterfacesCallbacks.GetWebSocket) context;
            getCentralData = (InterfacesCallbacks.GetCentralData) context;
            setCentralData = (InterfacesCallbacks.SetCentralData) context;
            fragmentInteraction = (InterfacesCallbacks.FragmentInteraction) context;
        } catch (ClassCastException cce) {
            throw new ClassCastException((context.toString() + "must implement onEventLogin"));
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        objectDriverData = getCentralData.getObjectDriverData();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_login, container, false);
        editTextShipment    = view.findViewById(R.id.editTextShipment);
        editTextPin         = view.findViewById(R.id.editTextPassword);
        buttonLogin         = view.findViewById(R.id.buttonLogin);
        toast               = Toast.makeText(getContext(), "", Toast.LENGTH_SHORT);
        buttonLogin.setOnClickListener(onClickLoginListener);
        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        fragmentInteraction.hideStatusBar();
        fragmentInteraction.hideNavigation();
    }
/*
    public void toggleKeyboard(String state) {
        if (state.contentEquals("show")) {
            inputMethodManager.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT);
        }
        if (state.contentEquals("hide")) {
            inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_IMPLICIT_ONLY);
        }
    }
*/
    private View.OnClickListener onClickLoginListener = new View.OnClickListener() {
        public void onClick(View view) {
            String inputShipment = editTextShipment.getText().toString();
            String inputPin = editTextPin.getText().toString();
            if (inputShipment.isEmpty()) {
                toast.setText("Bitte eine Transportnummer eingeben.");
                toast.show();
            } else if (inputShipment.equals(inputPin)) {
                objectDriverData.setShipment(inputShipment);
                fragmentInteraction.changeMainFragment("fragmentStatus");
                fragmentInteraction.showStatusBar();
                fragmentInteraction.showNavigation();
                fragmentInteraction.setNavigationItem(R.id.navigationOrder);
            } else {
                toast.setText("Login ungültig");
                toast.show();
            }
        }
    };

    /*
    private View.OnClickListener OnDateClickListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            DatePickerDialog datePickerDialog = new DatePickerDialog(getContext(),
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            pickedCalendar = Calendar.getInstance();
                            pickedCalendar.set(year, month, dayOfMonth);
                            buttonDate.setText(simpleDateFormat.format(pickedCalendar.getTime()));
                        }
                    }, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));
            datePickerDialog.show();
        }
    };
    */
}
