package com.intecio.timewindow;

import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class WebSocketWriteChatMessage extends WebSocketClient {

    private JSONObject response;
    private boolean readResponse = false;
    private ObjectChatMessage objectChatMessage;


    public WebSocketWriteChatMessage(URI serverUri) {
        super(serverUri);
        //this.objectChatMessage = objectChatMessage;
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        Log.e("WEBSOCKET", "WriteChatMessage onOpen");
    }

    @Override
    public void onMessage(String message) {
        Log.e("WRITECHATMESSAGE", message);
    }

    @Override
    public void onClose(int i, String s, boolean b) {

    }
    @Override
    public void onError(Exception e) {
        Log.e("WEBSOCKET", "WriteChatMessage onError:" + e.toString());
    }

}
