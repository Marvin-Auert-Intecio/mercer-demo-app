package com.intecio.timewindow;

public class ObjectDriverData {

    private String status = "";
    private String shipment = "";
    private String firstName = "";
    private String lastName = "";
    private String licensePlate = "";
    private String phone = "";
    private String trailerLicensePlate = "";
    private String dplbg = "";
    private String uplbg = "";
    private String dplend= "";
    private String uplend = "";
    private String spediteur = "";
    private String locale = "DE";
    private String yardNumber = "";

    public String getYardNumber() {
        return yardNumber;
    }

    public void setYardNumber(String yardNumber) {
        this.yardNumber = yardNumber;
    }

    public String getDplbg() {
        return dplbg;
    }

    public void setDplbg(String dplbg) {
        this.dplbg = dplbg;
    }

    public String getUplbg() {
        return uplbg;
    }

    public void setUplbg(String uplbg) {
        this.uplbg = uplbg;
    }

    public String getDplend() {
        return dplend;
    }

    public void setDplend(String dplend) {
        this.dplend = dplend;
    }

    public String getUplend() {
        return uplend;
    }

    public void setUplend(String uplend) {
        this.uplend = uplend;
    }

    public String getSpediteur() {
        return spediteur;
    }

    public void setSpediteur(String spediteur) {
        this.spediteur = spediteur;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public ObjectDriverData() {}

    public String getShipment() {
        return shipment;
    }

    public void setShipment(String shipment) {
        this.shipment = shipment;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getTrailerLicensePlate() {
        return trailerLicensePlate;
    }

    public void setTrailerLicensePlate(String trailerLicensePlate) {
        this.trailerLicensePlate = trailerLicensePlate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
