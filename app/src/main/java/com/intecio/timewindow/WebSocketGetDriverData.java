package com.intecio.timewindow;

import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class WebSocketGetDriverData extends WebSocketClient {

    private boolean readResponse = false;
    private JSONObject response;
    private JSONObject returnValue;
    private ObjectDriverData objectDriverData;
    private String date;
    private String time;
    private String name;
    private String chatMessage;
    private String shipment;
    private String firstName;
    private String lastName;
    private String licensePlate;
    private String phone;
    private String trailerLicensePlate;
    private String dplbg = "";
    private String uplbg = "";
    private String dplend= "";
    private String uplend = "";
    private String spediteur = "";
    private String locale = "DE";
    private String yardNumber = "";
    //private String status = "";

    public WebSocketGetDriverData(URI serverUri, ObjectDriverData objectDriverData) {
        super(serverUri);
        this.objectDriverData = objectDriverData;
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        Log.e("WEBSOCKET", "GetDriverData onOpen");
    }

    @Override
    public void onMessage(String message) {
        try {
            response = new JSONObject(message);
            returnValue = response.getJSONObject("returnValue");
            shipment = response.getString("shipment");
            firstName = response.getString("firstName");
            lastName = response.getString("lastName");
            licensePlate = response.getString("licensePlate");
            phone = response.getString("phone");
            dplbg = response.getString("dplbg");
            uplbg = response.getString("uplbg");
            dplend = response.getString("dplend");
            uplend = response.getString("uplend");
            spediteur = response.getString("spediteur");
            trailerLicensePlate = response.getString("trailerLicensePlate");
            yardNumber = response.getString("yardNumber");
            //status = response.getString("status");

            readResponse = true;
        } catch (JSONException e) {
            readResponse = false;
        }

        if (readResponse) {
            objectDriverData.setShipment(shipment);
            objectDriverData.setFirstName(firstName);
            objectDriverData.setLastName(lastName);
            objectDriverData.setLicensePlate(licensePlate);
            objectDriverData.setPhone(phone);
            objectDriverData.setDplbg(dplbg);
            objectDriverData.setUplbg(uplbg);
            objectDriverData.setDplend(dplend);
            objectDriverData.setUplend(uplend);
            objectDriverData.setSpediteur(spediteur);
            objectDriverData.setTrailerLicensePlate(trailerLicensePlate);
            objectDriverData.setYardNumber(yardNumber);
            if (objectDriverData.getStatus().contentEquals("")) {
                objectDriverData.setStatus("Freie Fahrt");
            }

        }
    }

    @Override
    public void onClose(int i, String s, boolean b) {

    }

    @Override
    public void onError(Exception e) {
        Log.e("WEBSOCKET", "LoadChatMessages onError:" + e.toString());
    }
}
