package com.intecio.timewindow;

import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class WebSocketLoadChatMessages extends WebSocketClient {

    private boolean readResponse = false;
    private boolean listReady = false;
    private int listLength = 0;
    private JSONArray responseArray;
    private JSONObject response;
    private JSONObject messageObject;
    private String date;
    private String time;
    private String name;
    private String chatMessage;
    private ObjectChatMessage objectChatMessage;
    private List<ObjectChatMessage> listObjectMessage;

    public WebSocketLoadChatMessages(URI serverUri) {
        super(serverUri);
        //this.objectChatMessage = objectChatMessage;
        //objectChatMessage = new ObjectChatMessage();
        this.listObjectMessage = new ArrayList<>();
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        Log.e("WEBSOCKET", "LoadChatMessages onOpen");
    }

    @Override
    public void onMessage(String message) {
        try {
            response = new JSONObject(message);
            responseArray = response.getJSONArray("chatMessages");
            readResponse = true;
        } catch (JSONException je) {
            readResponse = false;
        }
        if (readResponse) {
            if (listLength == 0) {
                try {
                    for (int i = responseArray.length() - 1; i >= 0; i--) {
                        messageObject = (JSONObject) responseArray.get(i);
                        objectChatMessage = new ObjectChatMessage();
                        objectChatMessage.setDate(messageObject.getString("date"));
                        objectChatMessage.setTime(messageObject.getString("time"));
                        objectChatMessage.setName(messageObject.getString("name"));
                        objectChatMessage.setChatMessage(messageObject.getString("message"));
                        listObjectMessage.add(objectChatMessage);
                        listReady = true;
                    }
                } catch (JSONException je) {

                }
            } else if (responseArray.length() != listLength) {
                try {
                    messageObject = (JSONObject) responseArray.get(0);
                    objectChatMessage = new ObjectChatMessage();
                    objectChatMessage.setDate(messageObject.getString("date"));
                    objectChatMessage.setTime(messageObject.getString("time"));
                    objectChatMessage.setName(messageObject.getString("name"));
                    objectChatMessage.setChatMessage(messageObject.getString("message"));
                    listObjectMessage.add(objectChatMessage);
                    listReady = true;
                } catch (JSONException e) {

                }
            } else {
                listReady = false;
            }
            listLength = responseArray.length();
        }
    }

    @Override
    public void onClose(int i, String s, boolean b) {

    }

    @Override
    public void onError(Exception e) {
        Log.e("WEBSOCKET", "LoadChatMessages onError:" + e.toString());
    }

    public List<ObjectChatMessage> getListObjectMessage() {
        return listObjectMessage;
    }

    public boolean getListReady() {
        return listReady;
    }
}
