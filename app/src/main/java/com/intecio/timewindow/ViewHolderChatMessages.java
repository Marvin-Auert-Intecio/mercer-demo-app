package com.intecio.timewindow;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class ViewHolderChatMessages extends RecyclerView.ViewHolder {

    public TextView chatMessage;
    public TextView name;
    public TextView time;
    public TextView date;

    public ViewHolderChatMessages(@NonNull View itemView) {
        super(itemView);
        chatMessage = itemView.findViewById(R.id.textViewChatMessage);
        name        = itemView.findViewById(R.id.textViewChatName);
        time        = itemView.findViewById(R.id.textViewChatTime);
        date        = itemView.findViewById(R.id.textViewChatDate);
    }
}
