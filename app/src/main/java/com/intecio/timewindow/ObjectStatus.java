package com.intecio.timewindow;

public class ObjectStatus {

    private String driver;
    private String plate;
    private String plateTrailer;
    private String shipmentComplete;
    private String shipmentIncomplete;
    private String timeWindow;
    private String destination;
    private String quantity;
    private String status;

    public ObjectStatus(String driver, String plate, String plateTrailer, String shipmentComplete,
                        String shipmentIncomplete, String timeWindow, String destination,
                        String quantity, String status) {
        this.driver = driver;
        this.plate = plate;
        this.plateTrailer = plateTrailer;
        this.shipmentComplete = shipmentComplete;
        this.shipmentIncomplete = shipmentIncomplete;
        this.timeWindow = timeWindow;
        this.destination = destination;
        this.quantity = quantity;
        this.status = status;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getPlate() {
        return plate;
    }

    public void setPlate(String plate) {
        this.plate = plate;
    }

    public String getPlateTrailer() {
        return plateTrailer;
    }

    public void setPlateTrailer(String plateTrailer) {
        this.plateTrailer = plateTrailer;
    }

    public String getShipmentComplete() {
        return shipmentComplete;
    }

    public void setShipmentComplete(String shipment) {
        this.shipmentComplete = shipment;
    }

    public String getShipmentIncomplete() {
        return shipmentIncomplete;
    }

    public void setShipmentIncomplete(String shipment) {
        this.shipmentIncomplete = shipment;
    }

    public String getTimeWindow() {
        return timeWindow;
    }

    public void setTimeWindow(String timeWindow) {
        this.timeWindow = timeWindow;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
