package com.intecio.timewindow;

import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class WebSocketSetDriverData extends WebSocketClient {

    private JSONObject messageObject;
    private ObjectDriverData objectDriverData;
    private String shipment;
    private String firstName;
    private String lastName;
    private String licensePlate;
    private String phoneNumber;
    private String trailerLicensePlate;
    private String dplbg = "";
    private String uplbg = "";
    private String dplend= "";
    private String uplend = "";
    private String spediteur = "";
    private String locale = "DE";
    private String responseType = "";
    private String toastMessage;

    public WebSocketSetDriverData(URI serverUri, ObjectDriverData objectDriverData) {
        super(serverUri);
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        Log.e("WEBSOCKET", "SetDriverData onOpen");
    }

    @Override
    public void onMessage(String message) {
        Log.e("SETDRIVERDATA", message);
        try {
            messageObject = new JSONObject(message);
            responseType = messageObject.getString("type");
            toastMessage = messageObject.getString("message");
        } catch (JSONException je) {

        }
    }

    @Override
    public void onClose(int i, String s, boolean b) {

    }

    @Override
    public void onError(Exception e) {
        Log.e("WEBSOCKET", "SetDriverData onError:" + e.toString());
    }

    public String getResponseType() {
        return responseType;
    }

    public void setResponseType(String responseType) {
        this.responseType = responseType;
    }
}
