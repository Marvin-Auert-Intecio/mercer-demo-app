package com.intecio.timewindow;

import android.util.Log;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class WebSocketLoadShipment extends WebSocketClient {

    private boolean readResponse = false;
    private DateTimeFormatter dateTimeFormatter;
    private int planned;
    private int unplanned;
    private int timeSlotSize;
    private int startHour;
    private int endHour;
    private int startMinute;
    private int endMinute;
    private int dateDay;
    private int dateMonth;
    private int dateYear;
    private int[] unplannedShipments;
    private JSONArray rows;
    private JSONArray unplannedJsonArray;
    private JSONArray appointmentArray;
    private JSONObject shipmentResponse;
    private JSONObject spediteur;
    private JSONObject titleObject;
    private JSONObject timeObject;
    private LocalDateTime startDateTime;
    private LocalDateTime endDateTime;
    private String startDate;
    private String returnMessage;
    private String returnType;
    private String jsonMessage;
    private String spediteur_BESCHREIBUNG;
    private String tknum;
    private String tdlnr;
    private String startString;
    private String endString;
    private String startTime;
    private String endTime;
    private String shipmentDay;
    private String shipmentMonth;
    private String shipmentYear;
    private String shipmentDate;

    public WebSocketLoadShipment(URI serverUri) {
        super(serverUri);
        dateTimeFormatter = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
    }

    @Override
    public void onOpen(ServerHandshake serverHandshake) {
        Log.e("WEBSOCKET", "LoadShipment onOpen");
    }

    @Override
    public void onMessage(String message) {
        try {
            shipmentResponse = new JSONObject(message);
            planned = shipmentResponse.getInt("planned");
            if (planned == 1){
                readResponse = true;
            }
        } catch (Exception e) {
            readResponse = false;
        }
        if (readResponse) {
            try {
                startDate = shipmentResponse.getString("startDate");
                returnMessage = shipmentResponse.getString("returnMessage");
                returnType = shipmentResponse.getString("returnType");
                unplanned = shipmentResponse.getInt("unplanned");
                unplannedJsonArray = shipmentResponse.getJSONArray("unplannedShipments");
                unplannedShipments = new int[unplannedJsonArray.length()];
                for (int i = 0; i < unplannedJsonArray.length(); i++) {
                    unplannedShipments[i] = unplannedJsonArray.getInt(i);
                }
                spediteur = shipmentResponse.getJSONObject("spediteur");
                spediteur_BESCHREIBUNG = spediteur.getString("spediteur_BESCHREIBUNG");
                tknum = spediteur.getString("tknum");
                tdlnr = spediteur.getString("tdlnr");
                rows = shipmentResponse.getJSONArray("rows");
                for (int i = 0; i < rows.length(); i++) {
                    titleObject = rows.getJSONObject(i);
                    if ((titleObject.getString("title").contains("Nr.:"))) {
                        timeSlotSize = titleObject.getInt("timeSlotSize");
                        appointmentArray = titleObject.getJSONArray("appointments");
                        for (int a = 0; a < appointmentArray.length(); a++) {
                            timeObject = appointmentArray.getJSONObject(a);
                            if (timeObject.getString("text").contentEquals(" Verladung")) {
                                startString = timeObject.getString("start");
                                endString = timeObject.getString("end");
                                Log.i("-", startString);
                                Log.i("-", endString);
                                String[] stringArrayStart = startString.split("\\+");
                                startString = stringArrayStart[0];
                                String[] stringArrayEnd = endString.split("\\+");
                                endString = stringArrayEnd[0];
                                try {
                                    startDateTime = LocalDateTime.parse(startString);
                                    startHour = startDateTime.getHour();
                                    startMinute = startDateTime.getMinute();
                                    startTime = String.format("%02d", startHour) + ":" + String.format("%02d", startMinute);


                                    endDateTime = LocalDateTime.parse(endString);
                                    endHour = endDateTime.getHour();
                                    endMinute = endDateTime.getMinute();
                                    endTime = String.format("%02d", endHour) + ":" + String.format("%02d", endMinute);


                                    dateDay = startDateTime.getDayOfMonth();
                                    dateMonth = startDateTime.getMonthValue();
                                    dateYear = startDateTime.getYear();

                                    shipmentDay = String.format("%02d", dateDay);
                                    shipmentMonth = String.format("%02d", dateMonth);
                                    shipmentYear = Integer.toString(dateYear);

                                    Log.i("-", startDateTime.toString());
                                    Log.i("-", endDateTime.toString());
                                    //Log.i("-", startHour + ":" + startMinute);
                                    //Log.i("-", endHour + ":" + endMinute);
                                } catch (Exception e) {
                                    Log.e("-", e.toString());
                                }
                            }
                        }
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClose(int i, String s, boolean b) {

    }

    @Override
    public void onError(Exception e) {
        Log.e("WEBSOCKET", "LoadShipment onError:" + e.toString());
    }

    public int getTimeSlotSize() {
        return timeSlotSize;
    }

    public String getStartTime() {
        return startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public String getShipmentDate() {
        shipmentDate = shipmentDay + "."
                + shipmentMonth + "."
                + shipmentYear;
        return shipmentDate;
    }

    public int getStartHour() {
        return startHour;
    }

    public int getEndHour() {
        return endHour;
    }

    public int getStartMinute() {
        return startMinute;
    }

    public int getEndMinute() {
        return endMinute;
    }

    public String getTdlnr() {
        return tdlnr;
    }
}
